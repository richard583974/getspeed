from math import sin, cos, sqrt, atan2, radians
import numpy as np
import time
import logging
import threading

def getDistance(nextX, nextY, lastX, lastY):
  Radius = 6373.0

  lat1 = radians(nextX)
  lon1 = radians(nextY)
  lat2 = radians(lastX)
  lon2 = radians(lastY)

  dlon = lon2 - lon1
  dlat = lat2 - lat1

  a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
  c = 2 * atan2(sqrt(a), sqrt(1 - a))

  dis = Radius * c # in km
  return dis*1000 # return in m



def calculateNext(nextX, nextY, lastX, lastY, prevTotalDistance, totalTime):
  distance = getDistance(nextX, nextY, lastX, lastY)
  currSpeed = distance
  currTotalDistance = prevTotalDistance + distance
  totalTime += 1
  averageSpeed = currTotalDistance / totalTime
  return averageSpeed, currSpeed, currTotalDistance, totalTime


def sendingPos(dat):
  counter = 1 # start from second element
  while (counter < len(dat)) : # stop before last row
    currPos = dat[counter]
    global nextX
    nextX = currPos[0]
    global nextY
    nextY = currPos[1]
    global hasNextPos
    hasNextPos = 1
    time.sleep(1)
    counter+=1
  global isFinish
  isFinish = 1

dat = np.loadtxt( 'missionwaypoints.txt' )
counter =1
totalDistance = 0
totalTime = 0
lastX = dat[0][0]
lastY = dat[0][1]

while counter < len(dat):
  currPos = dat[counter]
  nextX = currPos[0]
  nextY = currPos[1]
  averageSpeed, currSpeed, totalDistance, totalTime = calculateNext(nextX, nextY, lastX, lastY, totalDistance, totalTime)
  lastX = nextX
  lastY = nextY
  counter += 1
print('approximated overall speed for all points is (meter/sec):')
print(averageSpeed)
print('#######')

print('below are for showing current speed of each points')


counter = 1
totalDistance = 0
totalTime = 0
lastX = dat[0][0]
lastY = dat[0][1]
nextX = 0
nextY = 0
hasNextPos = 0
isFinish = 0

x = threading.Thread(target=sendingPos, args=(dat,))
x.start()
# created a thread to send one position per second
# runing again within short period of time without restart runtime might cause issue

while (isFinish == 0):
  while ((isFinish == 0) & (hasNextPos == 0)):
    time.sleep(0.1)
  if (isFinish == 0):
    hasNextPos=0  
    print('X value:')
    print(nextX)
    print('Y value:')
    print(nextY)
    averageSpeed, currSpeed, totalDistance, totalTime = calculateNext(nextX, nextY, lastX, lastY, totalDistance, totalTime)
    lastX = nextX
    lastY = nextY
    print('Second number:')
    print(totalTime)
    print('currSpeed for the second (meter/sec):')
    print(currSpeed)
    print('###')
    time.sleep(0.3)

